$(function (){
            $("[data-toggle='tooltip']").tooltip();
            $("[data-toggle='popover']").popover();
            $('.carousel').carousel({
                interval: 3000
            });

            $('#Contactar').on('show.bs.modal', function (e){
                console.log ('el modal Contactar se muestra');

                $('#contactarBtn').removeClass('btn-outline-primary');
                $('#contactarBtn').addClass('btn-outline-success');
                $('#contactarBtn').prop('disabled', true);

            });
            $('#Contactar').on('shown.bs.modal', function (e){
                console.log ('el modal Contactar se mostró');
            });
            $('#Contactar').on('hide.bs.modal', function (e){
                console.log ('el modal Contactar se oculta');
            });
            $('#Contactar').on('hidden.bs.modal', function (e){
                console.log ('el modal Contactar se ocultó');

                $('#contactarBtn').removeClass('btn-outline-success');
                $('#contactarBtn').addClass('btn-outline-primary');
                $('#contactarBtn').prop('disabled', false);
            });
        });